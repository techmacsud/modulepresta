{if isset($confirmation)}
    <div class="alert alert-succes">
        La configuration a bien été mise à jour
    </div>
{/if}
<form method="post" action="" class="defaultForm form-horizontal">
    <div class="panel">
        <div class="panel-heading">
            <i class="icon-cogs"></i>
            Configuration du module
        </div>
    </div>

    <div class="form-wrapper">
        <div class="form-group">
            <label class="control-label col-lg-3">Afficher les produits actifs :</label>
            <div class="col-lg-9">
                <input type="radio" id="enable_actif_1" name="enable_actif" value="1" {if $enable_actif eq '1'}checked{/if}/>
                <label for="enable_actif_1" class="t">oui</label>
                <input type="radio" id="enable_actif_0" name="enable_actif" value="0" {if $enable_actif ne '1'}checked{/if}/>
                <label for="enable_actif_0" class="t">non</label>
            </div>
        </div>
        <div class="panel-footer">
            <button class="btn btn-default pull-right" type="submit" name="submit_actif_form" value="1">
            <i class="process-icon-save"></i>Enregistrer
            </button>
        </div>
    </div>
</form>

{if $enable_actif eq 1}
<br>
<div class="text-center">
    <h2>
        Liste des produits actifs sur la boutique
    </h2>
    <div class="row justify-content-center">
        <div class="table-wrap col-lg-12">
            <table class="table table-bordered">
                <thead class="thead-light">
                <tr>
                    <th class="name text-center" scope="row">Référence du produit</th>
                    <th class="name text-center" scope="row">Nom du produit</th>
                    <th class="name text-center" scope="row">Prix du produit HT</th>
                    <th class="name text-center" scope="row">Prix du produit TTC</th>
                </tr>
                </thead>

                <tboby>
                    {foreach from=$productActifs item=result}
                    <tr>
                        <td class="name text-center">{$result.reference}</td>
                        <td class="name text-center">{$result.name}</td>
                        <td class="name text-center">{$result.priceht}</td>
                        <td class="name text-center">{$result.pricettc}</td>
                    </tr>
                    {/foreach}
                 </tboby>
            </table>
        </div>
    </div>
</div>
{/if}
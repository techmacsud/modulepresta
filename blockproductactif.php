<?php



if (!defined('_PS_VERSION_')) {
   exit;
}

/**
 * Class BlockProductActif
 * Création d'un module prestashop
 * test d'entretien pour afficher la liste des articles actifs sur la page d'accueil
 */
class BlockProductActif extends Module
{
   public function __construct()
   {
       $this->name = 'blockproductactif';
       $this->tab = 'front_office_features';
       $this->version = '1.0.0';
       $this->author = 'Jerome Pepermans';

       $this->displayName = $this->l('Produits actifs');
       $this->description = $this->l('Affiche les produits actifs de la boutique');
       $this->bootstrap = true;

       parent::__construct();
   }

    /**
     * Déclaration d'installation dans un hook
     * hook 'displayHomeTab' affiche sur la page d'accueil
     */
    public function install()
   {
       parent::install();
       $this->registerHook('displayHomeTab');

       return true;
   }

    /**
     * Méthode pour la configuration du module
     */
    public function processConfiguration()
   {
       if (Tools::isSubmit('submit_actif_form'))
       {
          $enable_actif = Tools::getValue('enable_actif');
          Configuration::updateValue('MYMOD_ACTIF', $enable_actif);
          $this->context->smarty->assign('confirmation', 'ok');
       }
   }

    /**
     * Méthode pour assigner la configuration aux boutons radio du module
     */
    public function assignConfiguration()
   {
       $enable_actif = Configuration::get('MYMOD_ACTIF');
       $this->context->smarty->assign('enable_actif', $enable_actif);
   }

    /**
     * @return mixed
     * renvoie à la vue
     */
    public function getContent()
   {
       $this->processConfiguration();
       $this->assignConfiguration();
       return $this->display(__FILE__, 'getContent.tpl');
   }

    /**
     * @param $param
     * @return string
     * Permet de greffer le module
     */
    public function hookDisplayHomeTab($param)
    {
        $this->displayProductActif();
        return $this->display(__FILE__,'blockproductactif.tpl');
    }

    /**
     * Récupération des données en BDD et envoie à la vue
     */
    public function displayProductActif()
    {
        $enable_actif = Configuration::get('MYMOD_ACTIF');
        $productActifs = Db::getInstance()->executeS('
        SELECT p.`reference` AS `reference`, pl.`name` AS name, ROUND(p.`price`, 2) AS `priceht`,  ROUND(p.`price` * 1.2, 2) AS `pricettc`
        FROM `'._DB_PREFIX_.'product_lang` pl
        LEFT JOIN `'._DB_PREFIX_.'product` p
            ON p.`id_product` = pl.`id_product`
        WHERE p.`active` = 1');

        $this->context->smarty->assign('enable_actif', $enable_actif);
        $this->context->smarty->assign('productActifs', $productActifs);
    }
}